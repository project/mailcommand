About
=====
This module can be used to execute Drupal functions through email.

Design
======
Dependencies
------------

The system depends on the framework created for og2list to receive
mail. As an alternative, mailhandler.module can be used. See INSTALL.txt.


Architecture
------------

A Drupal module will implement a hook which defines the commands which
it will accept and the actions it will execute upon receiving a
suitable command.

Operations:
The hook has the following operations:

'help': Some explanation sent back when help is requested.

'commands': The commands implemented by the module. It can happen that
several modules define the same command, e.g. 'subscribe'. In this
case you can not use the modules together,

Each command defines several arguments:

'action': The action taken for a particular command.

'arguments': The expected arguments with prescription for validation.

'authentification': The method used to identify the user. Possible
values: 'none', 'token', 'account', 'pgp'. Methods can be combined
and will be tried in sequence.

none: No confirmation neccessary. Use with care.

token: Generate a token and send back to the user. Requires an
existing Drupal account. Permissions will be checked based on email
address. A token will be generated.

pgp: Requires an account and a pgp key associated with it.

'permission': The Drupal permission required to execute the action.


Example:

function foo_mailcommand ($op) {
  switch ($op) {
    case 'help':
      return t('The foo module allows you to do subscribe and unsubscibe.');
    case 'commands':
      return array(
      	     'subscribe' => array(
			 'help' => t('This command allows you to subscribe. It takes two arguments: a list name and an email address. The latter is optional and defaults to the mail the command was received from.'),
			 'arguments' => array(
						array(
						'optional' => FALSE,
						'validate' => 'find_valid_list'
						),
						array(
						'optional' => TRUE,
						'validate' => 'valid_email_address',
						'default' => 'find_email_address'
						),
					),
			 'authenticate' => array('token'),
			 'action' => 'execute_subscription',
	     ),
	     'unsubscribe' => array(),
			 'help' => t('This command allows you to unsubscribe. It takes two arguments: a list name and an email address. The latter is optional and defaults to the mail the command was received from.'),
			 'arguments' => array(
						array(
						'optional' => FALSE,
						'validate' => 'find_valid_list',
						),
						array(
						'optional' => TRUE,
						'validate' => 'valid_email_address',
						'default' => 'find_email_address',
						),
					),
			 'authenticate' => array('account', 'pgp'),
			 'action' => 'execute_unsubscription',
			 'permission' => 'unsubscribe',
             );
  }
}


Workflow
--------

The intended workflow for the mail command system is as follows:

1) The user sends an email which contains the mail commands.

2) The mail is received and parsed for syntactic correctness

3) If the mail is syntactically correct, and the user is allowed to
take the requested action, the mail commands are stored and a token
is generated and send back to the user. Otherwise an error message is
generated and sent back. 

 Optional: 
 If the mail is encoded with a known PGP key, then the
 action can be executed immediately. This is an option to be
 implemented at a later stage.

4) If the token is sent back through either email or a web interface
within a specified time, the stored action is executed. A response
might be sent back.


Author
======

Gerhard Killesreiter <gerhard@killesreiter.de>


Sponsor
=======

CivicSpace

